const descripcion = {
    demand: true,
    alias: 'd',
    desc: 'Descripcion de la tarea.'
}
const completado = {
    default: true,
    alias: 'c',
    desc: 'Marca la tarea como completada/pendiente.'
}

const argv = require( 'yargs' )
    .command( 'crear', 'Crea una tarea.', { descripcion } )
    .command( 'actualizar', 'Actualiza una tarea.', { descripcion, completado } )
    .command( 'eliminar', 'Elimina una tarea.', { descripcion } )
    .command( 'listar', 'Visualiza las tareas.', { completado } )
    .help()
    .argv;

module.exports = {
    argv
}