const argv = require( './config/yargs' ).argv;
const { crear, getListado, actualizar, eliminar } = require( './tareas/tarea_pendiente' );
const colors = require( 'colors' );

let comando = argv._[0];

switch(comando) {
    case 'crear':
        console.log( crear( argv.descripcion ) );
        break;
    case 'listado':
        console.log( 'TAREAS POR HACER: '.red );
        for ( let tarea of getListado() ) {
            console.log( ( tarea.descripcion ).blue );
            console.log( ( 'Estado: ' + tarea.completado ).yellow );
        }
        break;
    case 'actualizar':
        console.log( actualizar( argv.descripcion, argv.completado) );
        break;
    case 'eliminar':
        console.log( eliminar( argv.descripcion ) )
        break;
    case 'listar':
        if( argv.completado === 'all' ){
            console.log( 'LISTA DE TAREAS: '.red );
            for ( let tarea of getListado() ) {
                console.log( ( tarea.descripcion ).blue );
                console.log( ( 'Estado: ' + tarea.completado ).yellow );
            }
        }
        else if( argv.completado === 'false' ){
            console.log( 'TAREAS POR HACER: '.red );
            for ( let tarea of getListado() ) {
                if( !tarea.completado ) {
                    console.log( ( tarea.descripcion ).blue );
                    console.log( ( 'Estado: ' + tarea.completado ).yellow );   
                }
            }
        }
        else if( argv.completado === 'true' ){
            console.log( 'TAREAS REALIZADAS: '.red );
            for ( let tarea of getListado() ) {
                if( tarea.completado ) {
                    console.log( ( tarea.descripcion ).blue );
                    console.log( ( 'Estado: ' + tarea.completado ).yellow );   
                }
            }
        }
        break;
    default:
        console.log("Comando no reconocido.");
        break;
}