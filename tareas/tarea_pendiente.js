const fs = require( 'fs' );
const NOMBRE_ARCHIVO = 'db/data.json';

let lista_tareas_pendientes = [];

const guardarDB = () => {
    let data = JSON.stringify( lista_tareas_pendientes );
    fs.writeFile( NOMBRE_ARCHIVO, data, (err) => {
        if (err) throw new Error( 'No se grabó.' );
    });
}

const crear = descripcion => {
    cargarDB();
    let tarea = {
        descripcion,
        completado: false
    }
    lista_tareas_pendientes.push( tarea );
    guardarDB();
    return tarea;
}

const cargarDB = () => {
    try {
        lista_tareas_pendientes = require( '../' + NOMBRE_ARCHIVO );
    } catch (error) {
        lista_tareas_pendientes = [];
    }
}

const getListado = () => {
    cargarDB();
    return lista_tareas_pendientes;
}

const actualizar = ( descripcion, completado ) => {
    cargarDB();
    let index = lista_tareas_pendientes.findIndex( tarea => tarea.descripcion === descripcion );
    if ( index >= 0 ) {
        lista_tareas_pendientes[index].completado = completado;
        guardarDB();
        return true;
    }
    return false;
}

const eliminar = descripcion => {
    cargarDB();
    let index = lista_tareas_pendientes.findIndex( tarea => tarea.descripcion === descripcion );
    if ( index >= 0 ) {
        lista_tareas_pendientes.splice( index, 1 );
        guardarDB();
        return true;
    }
    return false;
}

module.exports = {
    crear,
    getListado,
    actualizar,
    eliminar
}